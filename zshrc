# If you come from bash you might have to change your $
export PATH=/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/usr/share/oh-my-zsh"
# Default editor
export EDITOR="nvim"
# Compilation flags
export ARCHFLAGS="-arch $(uname -m)"
# SSH keys
export SSH_KEY_PATH="$HOME/.ssh/rsa_id"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="bolt"
ZSH_CUSTOM="/usr/share/zsh"

# Oh-My-Zsh plugins
plugins=(git zsh-256color zsh-autosuggestions zsh-syntax-highlighting)

source "$ZSH/oh-my-zsh.sh"


# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

alias nv="$EDITOR"
alias vi="$EDITOR"

# Web development
alias art="php artisan"

alias gulp="node node_modules/gulp/bin/gulp.js"

function hms() {
    pushd "$HOME/Homestead" > /dev/null
    vagrant "${@:1}"
    popd > /dev/null
}
